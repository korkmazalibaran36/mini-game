﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class kontrol : MonoBehaviour
{
    public int zi, level,gem,ziplamakontrol, skin;
    public bool zbool, ileribl, geribl;
    public Text gemtxt;
    public Button btnzip;
    public Renderer krenderer;
    public GameObject failpanel;

    void Start()
    {
        gem = PlayerPrefs.GetInt("gem");
        level = PlayerPrefs.GetInt("level");
        skin = PlayerPrefs.GetInt("skin");
        failpanel = GameObject.Find("FailCanvas");
    }


    void FixedUpdate()
    {
        krenderer = this.GetComponent<Renderer>();

        if (skin == 0)
        {
            krenderer.material.SetColor("_Color",Color.cyan);
        }
        if (skin == 1)
        {
            krenderer.material.SetColor("_Color", Color.yellow);
        }
        if (skin == 2)
        {
            krenderer.material.SetColor("_Color", Color.green);
        }
        if (skin == 3)
        {
            krenderer.material.SetColor("_Color", Color.red);
        }
        if (skin == 4)
        {
            krenderer.material.SetColor("_Color", Color.gray);
        }
        if (skin == 5)
        {
            krenderer.material.SetColor("_Color", Color.white);
        }
        gemtxt.text = gem.ToString();
        if (ileribl)
        {
            this.transform.Translate(0.25f, 0, 0);
        }
        if (geribl)
        {
            this.transform.Translate(-0.25f, 0, 0);

        }
        if (zi <= 0)
        {
            ziplamakontrol--;
            if (ziplamakontrol == 0)
            {
              btnzip.interactable = true;
                zbool = false;
            }

        }
        
        

        if (zbool)
        {
            this.transform.Translate(0, 0.18f, 0);
            zi--;
          
        }
    }
    public void zipla()
    {
        zbool = true;
        zi = 25;
        ziplamakontrol = 35;

       btnzip.interactable = false;
    }
    public void ileri()
    {
        ileribl = true;
    }
    public void ileribirak()
    {
        ileribl = false;
    }
    public void geri()
    {
        geribl = true;
    }
    public void geribirak()
    {
        geribl = false;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.name == "diken")
        {
            collision.gameObject.SetActive(false);
            failpanel.GetComponent<FailCanvas>().aktiflik = true;
        }
        if (collision.gameObject.name == "diken1")
        {
            Application.LoadLevel(2);
        }
        if (collision.gameObject.name == "finish")
        {
            level += 1;
            PlayerPrefs.SetInt("level", level);
            Application.LoadLevel(3);
        }
        if (collision.gameObject.name == "gem")
        {
            gem += 1;
            PlayerPrefs.SetInt("gem",gem);
            collision.gameObject.SetActive(false);
        }

    }
}
