﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveAds : MonoBehaviour
{
    public int rewarded;
    public GameObject satinalbutton;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        rewarded = PlayerPrefs.GetInt("Rewarded");
        if(rewarded == 1)
        {
            satinalbutton.gameObject.SetActive(false);
        }
    }
    public void removeads()
    {
        PlayerPrefs.SetInt("Rewarded", 1);
    }
}
