﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FailCanvas : MonoBehaviour
{
    public GameObject b1,b2,b3,b4,b5;
    public bool aktiflik;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (aktiflik)
        {
            b1.SetActive(true);
            b2.SetActive(true);
            b3.SetActive(true);
            b4.SetActive(true);
            b5.SetActive(true);
        }
        else  if (!aktiflik)
        {
            b1.SetActive(false);
            b2.SetActive(false);
            b3.SetActive(false);
            b4.SetActive(false);
            b5.SetActive(false);
        }
    }
}
